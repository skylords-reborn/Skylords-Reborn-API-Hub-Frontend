FROM docker.io/node:lts-bullseye-slim AS build

WORKDIR /app
COPY . /app

RUN rm -rf .env
RUN mv .env.prod .env
RUN npm ci
RUN npm run build:prod

FROM docker.io/nginx:1.21.6-alpine AS app

WORKDIR /usr/share/nginx

COPY --from=build /app/dist html/
COPY ./nginx /etc/nginx

ENV NGINX_ENVSUBST_OUTPUT_DIR=/etc/nginx
ENV NGINX_ENVSUBST_TEMPLATE_DIR=/etc/nginx

EXPOSE 5000