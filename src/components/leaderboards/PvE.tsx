import { useEffect, useState } from "react";
import { Button, notification, Radio, Select, Table, Tag } from "antd";
import {
  NumberOutlined,
  ArrowLeftOutlined,
  ExportOutlined,
  ReloadOutlined,
} from "@ant-design/icons";
import axios, { CancelTokenSource } from "axios";
import { LeaderboardsPvETableRow, NameValue } from "../utils/Types";
import { LeaderboardsPvETableColumns } from "../utils/TableColumns";
import { GetSpinProps, m, setPath } from "../utils/Functions";
import { AxiosInstance } from "../utils/Axios";
import { LeaderboardsUrls, FrontendUrls } from "../utils/Urls";
import LeaderboardsMenu from "./LeaderboardsMenu";
import { PvEDefaults } from "../utils/Enums";

const { Option } = Select;

const PvE = ({ type, setActiveComponent, onClickCard }: any) => {
  const resultsPerPage = 20;
  const mapSelectorClasses =
    type === 1 || type === 12
      ? "map-selector-single-player mt-1"
      : "map-selector-multiple-players mt-1";
  const defaultSettings = PvEDefaults.find((d) => d.type === type);
  const [map, setMap] = useState<number>(
    defaultSettings ? defaultSettings.defMap : 0
  );
  const [players] = useState<NameValue[]>(
    defaultSettings ? defaultSettings.playerCounts : []
  );
  const [player, setPlayer] = useState<number>(
    defaultSettings ? defaultSettings.playerCounts[0].value : 0
  );
  const [loading, setLoading] = useState(false);
  const [tableRows, setTableRows] = useState<LeaderboardsPvETableRow[]>([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [numberOfResults, setNumberOfResults] = useState(0);
  const [timeRanges, setTimeRanges] = useState<NameValue[]>([]);
  const [timeRange, setTimeRange] = useState(0);
  const [maps, setMaps] = useState<NameValue[]>([]);

  useEffect(() => {
    let mounted = true;
    const source = axios.CancelToken.source();
    (async () => {
      if (mounted) {
        await search(1, mounted, source);
      }
    })();
    return () => {
      mounted = false;
      source.cancel();
    };
  }, [timeRange, map, player]);

  const onChange = async (page: number, pageSize?: number | undefined) => {
    if (page && pageSize) {
      setCurrentPage(page);
      await search(page);
    }
  };

  const search = async (
    page: number,
    mounted: boolean | undefined = undefined,
    source: CancelTokenSource | undefined = undefined
  ) => {
    if (mounted !== undefined && mounted === false) {
      return;
    }

    setLoading(true);
    try {
      let result = await AxiosInstance.get(
        `${process.env.REACT_APP_BE_ROOT_URL}${LeaderboardsUrls.PVE_RANGES}`,
        mounted !== undefined && source !== undefined
          ? { cancelToken: source.token }
          : {}
      );

      if (result && result.status === 200 && result.data) {
        setTimeRanges(result.data);
      }

      result = await AxiosInstance.get(
        `${process.env.REACT_APP_BE_ROOT_URL}${LeaderboardsUrls.PVE_MAPS}?type=${type}`,
        mounted !== undefined && source !== undefined
          ? { cancelToken: source.token }
          : {}
      );

      if (result && result.status === 200 && result.data) {
        setMaps(result.data);
      }

      result = await AxiosInstance.get(
        `${process.env.REACT_APP_BE_ROOT_URL}${LeaderboardsUrls.PVE_COUNT}?type=${type}&range=${timeRange}&players=${player}&map=${map}`,
        mounted !== undefined && source !== undefined
          ? { cancelToken: source.token }
          : {}
      );

      if (result && result.status === 200 && result.data) {
        const { count } = result.data;
        setNumberOfResults(count);

        result = await AxiosInstance.get(
          `${process.env.REACT_APP_BE_ROOT_URL}${LeaderboardsUrls.PVE_LEADERBOARDS}?type=${type}&range=${timeRange}&players=${player}&map=${map}&page=${page}&number=${resultsPerPage}`,
          mounted !== undefined && source !== undefined
            ? { cancelToken: source.token }
            : {}
        );

        if (result && result.status === 200 && result.data) {
          const rows = result.data as LeaderboardsPvETableRow[];
          if (rows) {
            for (let i = 0; i < rows.length; i++) {
              rows[i].key = rows[i].time + rows[i].difficultyStr;
              for (let j = 0; j < rows[i].players.length; j++) {
                rows[i].key += rows[i].players[j];
              }
            }
            setTableRows(rows);
          }
        }
      }
    } catch (error) {
      if (axios.isCancel(error) === false) {
        console.error(error);
      }
      setTableRows([]);
      setCurrentPage(1);
      setNumberOfResults(0);
    }
    setLoading(false);
  };

  const exportToCsv = async () => {
    try {
      const result = await AxiosInstance.get(
        `${process.env.REACT_APP_BE_ROOT_URL}${LeaderboardsUrls.PVE_LEADERBOARDS}?type=${type}&players=${player}&map=${map}&range=${timeRange}&export=true`
      );
      if (result && result.status === 201 && result.data) {
        const element = document.createElement("a");
        const file = new Blob([result.data], {
          type: "text/csv",
        });
        element.href = URL.createObjectURL(file);
        element.download = `sr-leaderboards-export-${type}P-${timeRange}.csv`;
        document.body.appendChild(element);
        element.click();
        element.remove();
      }
    } catch {
      notification.error(m("Failed to export leaderboards to CSV file."));
    }
  };

  const onChangeRadio = async (e: any) => {
    setTimeRange(e.target.value as number);
  };

  const onChangeMap = (e: any) => {
    if (e) {
      const map = maps.find(
        (f: NameValue) => f.value === parseInt(e)
      ) as NameValue;
      if (map) {
        setMap(map.value);
      }
    }
  };

  const onChangePlayers = (e: any) => {
    if (e) {
      const player = players.find(
        (f: NameValue) => f.value === parseInt(e)
      ) as NameValue;
      if (player) {
        setPlayer(player.value);
      }
    }
  };

  const TitleComponent = (): JSX.Element => {
    return (
      <div>
        <Button
          title="Go Back"
          size="small"
          type="primary"
          className="mt-1 my-bg bg-border mr-1"
          onClick={() => {
            setActiveComponent(<LeaderboardsMenu onClickCard={onClickCard} />);
            setPath(FrontendUrls.Leaderboards);
          }}
        >
          <ArrowLeftOutlined />
        </Button>
        <Button
          title="Export to CSV"
          size="small"
          type="primary"
          className="mt-1 ml-1 mr-1 my-bg bg-border"
          onClick={exportToCsv}
        >
          <ExportOutlined />
        </Button>
        <Button
          title="Refresh Table"
          size="small"
          type="primary"
          className="ml-1 mr-1 my-bg bg-border"
          onClick={() => search(currentPage)}
        >
          <ReloadOutlined />
        </Button>
        <Radio.Group
          onChange={onChangeRadio}
          value={timeRange}
          className="mt-1"
        >
          {timeRanges.map((range: NameValue) => {
            return (
              <Radio value={range.value} key={range.name}>
                {range.name}
              </Radio>
            );
          })}
        </Radio.Group>
        <Select
          onChange={onChangeMap}
          value={map}
          className={mapSelectorClasses}
        >
          {Object.keys(maps).map((idx: any) => {
            const map = maps[idx] as any;
            return (
              <Option key={map.name} value={map.value}>
                {map.name}
              </Option>
            );
          })}
        </Select>
        {players.length === 1 ? undefined : (
          <Select onChange={onChangePlayers} value={player} className="mt-1">
            {Object.keys(players).map((idx: any) => {
              const player = players[idx] as any;
              return (
                <Option key={player.name} value={player.value}>
                  {player.name}
                </Option>
              );
            })}
          </Select>
        )}
      </div>
    );
  };

  return (
    <Table
      title={() => <TitleComponent />}
      dataSource={tableRows as any}
      columns={LeaderboardsPvETableColumns}
      loading={loading ? GetSpinProps("LEADERBOARDS") : false}
      pagination={{
        className: "pagination my-bg text-white",
        onChange: onChange,
        pageSize: resultsPerPage,
        showSizeChanger: false,
        current: currentPage,
        total: numberOfResults,
        showTotal: () => (
          <Tag closable={false} title="Total number of results">
            <NumberOutlined /> {numberOfResults}
          </Tag>
        ),
        position: ["bottomCenter"],
      }}
    />
  );
};

export default PvE;
