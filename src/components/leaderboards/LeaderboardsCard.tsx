import { Card, Image } from "antd";

const { Meta } = Card;

const LeaderboardsCard = ({
  customKey,
  title,
  onClick,
  number,
  imgSrc,
}: any) => {
  const imgAlt = `${customKey} ${title} ${number}`;
  const metaTitle = `Table #${number}`;
  return (
    <Card
      onClick={onClick}
      title={title}
      key={customKey}
      hoverable
      className="my-card"
      cover={<Image alt={imgAlt} src={imgSrc} preview={false} />}
    >
      <Meta
        title={metaTitle}
        description="Click here to show the rankings table."
      />
    </Card>
  );
};

export default LeaderboardsCard;
