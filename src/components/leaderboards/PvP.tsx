import { useEffect, useState } from "react";
import { Button, notification, Radio, Table, Tag } from "antd";
import {
  NumberOutlined,
  ArrowLeftOutlined,
  ExportOutlined,
  ReloadOutlined,
} from "@ant-design/icons";
import axios, { CancelTokenSource } from "axios";
import {
  Leaderboards1v1TableRow,
  Leaderboards2v2TableRow,
  NameValue,
} from "../utils/Types";
import {
  Leaderboards1v1TableColumns,
  Leaderboards2v2TableColumns,
} from "../utils/TableColumns";
import { GetSpinProps, m, setPath } from "../utils/Functions";
import { AxiosInstance } from "../utils/Axios";
import { LeaderboardsUrls, FrontendUrls } from "../utils/Urls";
import LeaderboardsMenu from "./LeaderboardsMenu";

const PvP = ({ type, setActiveComponent, onClickCard }: any) => {
  const resultsPerPage = 20;
  const columns =
    type === "1"
      ? Leaderboards1v1TableColumns
      : type === "2"
      ? Leaderboards2v2TableColumns
      : undefined;
  const [loading, setLoading] = useState(false);
  const [tableRows, setTableRows] = useState<
    Leaderboards1v1TableRow[] | Leaderboards2v2TableRow[]
  >([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [numberOfResults, setNumberOfResults] = useState(0);
  const [timeRanges, setTimeRanges] = useState<NameValue[]>([]);
  const [timeRange, setTimeRange] = useState(0);

  useEffect(() => {
    let mounted = true;
    const source = axios.CancelToken.source();
    (async () => {
      if (mounted) {
        await search(1, mounted, source);
      }
    })();
    return () => {
      mounted = false;
      source.cancel();
    };
  }, [timeRange]);

  const onChange = async (page: number, pageSize?: number | undefined) => {
    if (page && pageSize) {
      setCurrentPage(page);
      await search(page);
    }
  };

  const search = async (
    page: number,
    mounted: boolean | undefined = undefined,
    source: CancelTokenSource | undefined = undefined
  ) => {
    if (mounted !== undefined && mounted === false) {
      return;
    }

    setLoading(true);
    try {
      let result = await AxiosInstance.get(
        `${process.env.REACT_APP_BE_ROOT_URL}${LeaderboardsUrls.PVP_RANGES}`,
        mounted !== undefined && source !== undefined
          ? { cancelToken: source.token }
          : {}
      );

      if (result && result.status === 200 && result.data) {
        setTimeRanges(result.data);
      }

      result = await AxiosInstance.get(
        `${process.env.REACT_APP_BE_ROOT_URL}${LeaderboardsUrls.PVP_COUNT}?type=${type}&range=${timeRange}`,
        mounted !== undefined && source !== undefined
          ? { cancelToken: source.token }
          : {}
      );

      if (result && result.status === 200 && result.data) {
        const { count } = result.data;
        setNumberOfResults(count);

        result = await AxiosInstance.get(
          `${process.env.REACT_APP_BE_ROOT_URL}${LeaderboardsUrls.PVP_LEADERBOARDS}?type=${type}&range=${timeRange}&page=${page}&number=${resultsPerPage}`,
          mounted !== undefined && source !== undefined
            ? { cancelToken: source.token }
            : {}
        );

        if (result && result.status === 200 && result.data) {
          if (type === "1") {
            const rows = result.data as Leaderboards1v1TableRow[];
            if (rows) {
              for (let i = 0; i < rows.length; i++) {
                rows[i].key = rows[i].players[0] + rows[i].rating;
              }
              setTableRows(rows);
            }
          } else {
            const rows = result.data as Leaderboards2v2TableRow[];
            if (rows) {
              for (let i = 0; i < rows.length; i++) {
                rows[i].key =
                  rows[i].players[0] + rows[i].players[1] + rows[i].rating;
              }
              setTableRows(rows);
            }
          }
        }
      }
    } catch (error) {
      if (axios.isCancel(error) === false) {
        console.error(error);
      }
      setTableRows([]);
      setCurrentPage(1);
      setNumberOfResults(0);
    }
    setLoading(false);
  };

  const exportToCsv = async () => {
    try {
      const result = await AxiosInstance.get(
        `${process.env.REACT_APP_BE_ROOT_URL}${LeaderboardsUrls.PVP_LEADERBOARDS}?type=${type}&range=${timeRange}&export=true`
      );
      if (result && result.status === 201 && result.data) {
        const element = document.createElement("a");
        const file = new Blob([result.data], {
          type: "text/csv",
        });
        element.href = URL.createObjectURL(file);
        element.download = `sr-leaderboards-export-${type}-${timeRange}.csv`;
        document.body.appendChild(element);
        element.click();
        element.remove();
      }
    } catch {
      notification.error(m("Failed to export leaderboards to CSV file."));
    }
  };

  const onChangeRadio = async (e: any) => {
    setTimeRange(e.target.value as number);
  };

  const TitleComponent = (): JSX.Element => {
    return (
      <div>
        <Button
          title="Go Back"
          size="small"
          type="primary"
          className="my-bg bg-border mr-1"
          onClick={() => {
            setActiveComponent(<LeaderboardsMenu onClickCard={onClickCard} />);
            setPath(FrontendUrls.Leaderboards);
          }}
        >
          <ArrowLeftOutlined />
        </Button>
        <Button
          title="Export to CSV"
          size="small"
          type="primary"
          className="ml-1 mr-1 my-bg bg-border"
          onClick={exportToCsv}
        >
          <ExportOutlined />
        </Button>
        <Button
          title="Refresh Table"
          size="small"
          type="primary"
          className="ml-1 mr-1 my-bg bg-border"
          onClick={() => search(currentPage)}
        >
          <ReloadOutlined />
        </Button>
        <Radio.Group onChange={onChangeRadio} value={timeRange}>
          {timeRanges.map((range: NameValue) => {
            return (
              <Radio value={range.value} key={range.name}>
                {range.name}
              </Radio>
            );
          })}
        </Radio.Group>
      </div>
    );
  };

  return (
    <Table
      title={() => <TitleComponent />}
      dataSource={tableRows as any}
      columns={columns}
      loading={loading ? GetSpinProps("LEADERBOARDS") : false}
      pagination={{
        className: "pagination my-bg text-white",
        onChange: onChange,
        pageSize: resultsPerPage,
        showSizeChanger: false,
        current: currentPage,
        total: numberOfResults,
        showTotal: () => (
          <Tag closable={false} title="Total number of results">
            <NumberOutlined /> {numberOfResults}
          </Tag>
        ),
        position: ["bottomCenter"],
      }}
    />
  );
};

export default PvP;
