import { Row } from "antd";
import { LeaderboardsKeys } from "../utils/Enums";
import LeaderboardsCard from "./LeaderboardsCard";

const LeaderboardsMenu = ({ onClickCard }: any) => {
  return (
    <>
      <Row>
        <LeaderboardsCard
          customKey={LeaderboardsKeys.PvE1}
          title="1-player PvE"
          onClick={() => onClickCard(LeaderboardsKeys.PvE1)}
          number={1}
          imgSrc={"/images/1pve.jpg"}
        />
        <LeaderboardsCard
          customKey={LeaderboardsKeys.PvE2}
          title="2-player PvE"
          onClick={() => onClickCard(LeaderboardsKeys.PvE2)}
          number={2}
          imgSrc={"/images/2pve.jpg"}
        />
        <LeaderboardsCard
          customKey={LeaderboardsKeys.PvE4}
          title="4-player PvE"
          onClick={() => onClickCard(LeaderboardsKeys.PvE4)}
          number={3}
          imgSrc={"/images/4pve.jpg"}
        />
      </Row>
      <Row>
        <LeaderboardsCard
          customKey={LeaderboardsKeys.PvE12}
          title="12-player PvE"
          onClick={() => onClickCard(LeaderboardsKeys.PvE12)}
          number={4}
          imgSrc={"/images/12pve.jpg"}
        />
        <LeaderboardsCard
          customKey={LeaderboardsKeys.PvP1}
          title="1v1 PvP"
          onClick={() => onClickCard(LeaderboardsKeys.PvP1)}
          number={5}
          imgSrc={"/images/1v1.jpg"}
        />
        <LeaderboardsCard
          customKey={LeaderboardsKeys.PvP2}
          title="2v2 PvP"
          onClick={() => onClickCard(LeaderboardsKeys.PvP2)}
          number={6}
          imgSrc={"/images/2v2.jpg"}
        />
      </Row>
    </>
  );
};

export default LeaderboardsMenu;
