import { useState, useEffect } from "react";
import axios, { AxiosResponse } from "axios";
import { Button, Table } from "antd";
import { ReloadOutlined } from "@ant-design/icons";
import { StatsUrls } from "./utils/Urls";
import { AxiosInstance } from "./utils/Axios";
import { StatsTableRow } from "./utils/Types";
import { GetStatsTableRows, GetSpinProps } from "./utils/Functions";
import { StatsTableColumns } from "./utils/TableColumns";

const Statistics = () => {
  const [trigger, setTrigger] = useState(0);
  const [tableRows, setTableRows] = useState<StatsTableRow[]>(
    GetStatsTableRows()
  );
  const [loading, setLoading] = useState(true);
  let source = axios.CancelToken.source();

  useEffect(() => {
    let mounted = true;
    (async () => {
      const rows: StatsTableRow[] = [];
      const promises: Promise<AxiosResponse<any>>[] = [];
      for (let i = 0; i < StatsUrls.length; i++) {
        if (!mounted) break;
        const su = StatsUrls[i];
        try {
          const result = AxiosInstance.get(
            `${process.env.REACT_APP_BE_ROOT_URL}${su.name}`,
            { cancelToken: source.token }
          );
          promises.push(result);
        } catch (error) {
          if (axios.isCancel(error) === false) {
            console.error(error);
          }
        }
      }
      try {
        const results = await Promise.all(promises);
        for (let i = 0; i < results.length; i++) {
          const result = results[i];
          const su = StatsUrls[i];
          if (result && result.status === 200 && result.data) {
            const { count } = result.data;
            const row: StatsTableRow = {
              name: su.value,
              count: parseInt(count),
              key: su.value + "+" + count,
            };
            rows.push(row);
          }
        }
      } catch (error) {
        if (axios.isCancel(error) === false) {
          console.error(error);
        }
      }
      if (mounted) {
        setTableRows(rows);
        setLoading(false);
      }
    })();
    return () => {
      mounted = false;
      source.cancel();
    };
  }, [trigger]);

  const triggerRefresh = () => {
    source = axios.CancelToken.source();
    setLoading(true);
    setTrigger(trigger + 1);
  };

  const GetStatsTableColumns = () => {
    const tc = StatsTableColumns[1] as any;
    tc.title = () => {
      return (
        <div className="d-inline-flex mt-14px">
          <p>COUNT</p>
          <Button
            size="small"
            title="Refresh Table"
            type="primary"
            className="ml-1 my-bg-2 bg-border"
            onClick={() => triggerRefresh()}
          >
            <ReloadOutlined />
          </Button>
        </div>
      );
    };
    return [StatsTableColumns[0], tc];
  };

  return (
    <Table
      dataSource={tableRows as any}
      columns={GetStatsTableColumns()}
      loading={loading ? GetSpinProps("STATS") : false}
      pagination={false}
    />
  );
};

export default Statistics;
