import { Row } from "antd";
import { InfoUrls } from "./utils/Urls";
import DocumentationCard from "./other/DocumentationCard";

const Documentation = () => {
  return (
    <Row>
      <DocumentationCard
        title={"Swagger API Documentation"}
        customKey="api-docs"
        number={1}
        url={InfoUrls.ApiDocs}
      />
    </Row>
  );
};

export default Documentation;
