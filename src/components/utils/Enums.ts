import { PvEDefault } from "./Types";

export enum MenuKeys {
  HOME = "1",
  API_STATS = "2",
  API_AUCTIONS = "3",
  API_RANKINGS = "4",
  DOCUMENTATION = "5",
}

export enum DocumentationKeys {
  STATS = "stats",
  AUCTIONS = "auctions",
  RANKINGS = "rankings",
  INFO = "info",
}

export enum LeaderboardsKeys {
  PvE1 = "1p-pve",
  PvE2 = "2p-pve",
  PvE4 = "4p-pve",
  PvE12 = "12p-pve",
  PvP1 = "1v1-pvp",
  PvP2 = "2v2-pvp",
}

export const PvEDefaults: PvEDefault[] = [
  {
    type: 1,
    defMap: 67,
    playerCounts: [{ name: "1", value: 1 }],
  },
  {
    type: 2,
    defMap: 9,
    playerCounts: [
      { name: "2", value: 2 },
      { name: "1", value: 1 },
    ],
  },
  {
    type: 4,
    defMap: 10,
    playerCounts: [
      { name: "4", value: 4 },
      { name: "3", value: 3 },
      { name: "2", value: 2 },
      { name: "1", value: 1 },
    ],
  },
  {
    type: 12,
    defMap: 37,
    playerCounts: [{ name: "12", value: 12 }],
  },
];
