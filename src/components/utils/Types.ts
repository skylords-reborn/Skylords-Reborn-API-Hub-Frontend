export type StatsTableRow = {
  name: string;
  count: number | string;
  key: string;
};

export type AuctionsTableRow = {
  auctionId: number;
  cardName: string;
  startingPrice: number;
  currentPrice: number;
  buyoutPrice: number;
  endingOn: string;
  key: string;
};

export type Leaderboards1v1TableRow = {
  players: string[];
  rating: number;
  activity: number;
  totalMatches: number;
  wins: number;
  baseElo: number;
  key: string;
};

export type Leaderboards2v2TableRow = {
  players: string[];
  rating: number;
  activity: number;
  wins: number;
  baseElo: number;
  key: string;
};

export type LeaderboardsPvETableRow = {
  players: string[];
  time: number;
  difficultyStr: string;
  key: string;
};

export type AuctionSearchFilters = {
  cardName: string;
  min: number;
  max: number;
};

export type NameValue = {
  name: string;
  value: number;
};

export type PvEDefault = {
  type: number;
  defMap: number;
  playerCounts: NameValue[];
};
