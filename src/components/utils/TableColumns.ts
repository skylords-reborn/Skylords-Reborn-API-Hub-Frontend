import { NumberWithCommas } from "./Functions";

export const StatsTableColumns = [
  {
    title: "STAT",
    dataIndex: "name",
    key: "STAT-name",
  },
  {
    title: "COUNT",
    dataIndex: "count",
    key: "COUNT-count",
    render: (value: any) => {
      return NumberWithCommas(value as number);
    },
  },
];

export const AuctionsTableColumns = [
  {
    title: "CARD",
    dataIndex: "cardName",
    key: "AUCT-card",
  },
  {
    title: "START",
    dataIndex: "startingPrice",
    key: "AUCT-start",
    render: (value: any) => {
      return NumberWithCommas(value as number);
    },
  },
  {
    title: "BID",
    dataIndex: "currentPrice",
    key: "AUCT-bid",
    render: (value: any) => {
      return NumberWithCommas(value as number);
    },
  },
  {
    title: "BUYOUT",
    dataIndex: "buyoutPrice",
    key: "AUCT-buy",
    render: (value: any) => {
      return NumberWithCommas(value as number);
    },
  },
  {
    title: "END",
    dataIndex: "endingOn",
    key: "AUCT-end",
  },
];

export const Leaderboards1v1TableColumns = [
  {
    title: "PLAYER",
    dataIndex: "players",
    key: "PLAYER-players",
    render: (value: any) => {
      const arr = value as string[];
      return `${arr[0]}`;
    },
  },
  {
    title: "RATING",
    dataIndex: "rating",
    key: "RATING-rating",
    render: (value: any) => {
      return NumberWithCommas(Math.ceil(value as number));
    },
  },
  {
    title: "ELO",
    dataIndex: "baseElo",
    key: "ELO-baseElo",
    render: (value: any) => {
      return NumberWithCommas(value as number);
    },
  },
  {
    title: "ACTIVITY",
    dataIndex: "activity",
    key: "ACTIVITY-activity",
    render: (value: any) => {
      return `${Math.ceil(value)}%`;
    },
  },
  {
    title: "MATCHES",
    dataIndex: "matches",
    key: "MATCHES-matches",
  },
  {
    title: "WINS",
    dataIndex: "wins",
    key: "WINS-wins",
  },
];

export const Leaderboards2v2TableColumns = [
  {
    title: "PLAYERS",
    dataIndex: "players",
    key: "PLAYERS-players",
    render: (value: any) => {
      const arr = value as string[];
      return `${arr[0]}, ${arr[1]}`;
    },
  },
  {
    title: "RATING",
    dataIndex: "rating",
    key: "RATING-rating",
    render: (value: any) => {
      return NumberWithCommas(Math.ceil(value as number));
    },
  },
  {
    title: "ELO",
    dataIndex: "baseElo",
    key: "ELO-baseElo",
    render: (value: any) => {
      return NumberWithCommas(value as number);
    },
  },
  {
    title: "ACTIVITY",
    dataIndex: "activity",
    key: "ACTIVITY-activity",
    render: (value: any) => {
      return `${Math.ceil(value)}%`;
    },
  },
  {
    title: "WINS",
    dataIndex: "wins",
    key: "WINS-wins",
  },
];

export const LeaderboardsPvETableColumns = [
  {
    title: "PLAYERS",
    dataIndex: "players",
    key: "PLAYERS-players",
    render: (value: any) => {
      const arr = value as string[];
      let name = "";
      for (let i = 0; i < arr.length; i++) {
        name += arr[i] + ", ";
      }
      name = name.substring(0, name.length - 2);
      return name;
    },
  },
  {
    title: "TIME",
    dataIndex: "time",
    key: "TIME-time",
    render: (value: any) => {
      const time = value as number;
      const min = Math.floor(time / 10 / 60);
      const minStr = min < 10 ? "0" + min : min;
      const sec = Math.floor(time / 10) % 60;
      const secStr = sec < 10 ? "0" + sec : sec;
      const milli = Math.floor(time % 10);
      return `${minStr}:${secStr}.${milli}`;
    },
  },
  {
    title: "DIFFICULTY",
    dataIndex: "difficultyStr",
    key: "DIFFICULTY-difficultyStr",
  },
];
