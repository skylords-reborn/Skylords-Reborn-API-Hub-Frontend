import { SpinProps } from "antd";
import { ArgsProps } from "antd/lib/notification";
import { StatsTableRow } from "./Types";
import { StatsUrls } from "./Urls";

export const GetStatsTableRows = (): StatsTableRow[] => {
  const arr: StatsTableRow[] = [];
  StatsUrls.map((row) => {
    const tr: StatsTableRow = {
      name: row.value,
      count: "Loading...",
      key: row.value + "-0",
    };
    return arr.push(tr);
  });
  return arr;
};

export const GetSpinProps = (word: string): SpinProps => {
  return {
    tip: `LOADING ${word}...`,
    size: "large",
    className: "text-black-bold",
  };
};

export const NumberWithCommas = (x: number): string => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

export const m = (message: string): ArgsProps => {
  return {
    message: message,
    duration: 5,
  };
};

export const setPath = (url: string): void => {
  window.history.replaceState(null, document.title, url);
};

export const getPageQueryParam = (): string => {
  const search = window.location.search.replace("?", "");
  const split = search.split("&");
  let link = "?";
  for (let i = 0; i < split.length; i++) {
    const s = split[i];
    if (s.startsWith("page=")) {
      link += s;
      break;
    }
  }
  return link;
};
