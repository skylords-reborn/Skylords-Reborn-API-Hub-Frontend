export enum FrontendUrls {
  Home = "?page=home",
  Statistics = "?page=stats",
  Auctions = "?page=auctions",
  Leaderboards = "?page=rankings",
  Documentation = "?page=docs",
  Leaderboards_1PvE = "?page=1p-pve",
  Leaderboards_2PvE = "?page=2p-pve",
  Leaderboards_4PvE = "?page=4p-pve",
  Leaderboards_12PvE = "?page=12p-pve",
  Leaderboards_1v1 = "?page=1v1-pvp",
  Leaderboards_2v2 = "?page=2v2-pvp",
}

export enum InfoUrls {
  ApiDocs = "/api/docs",
  HomeDocs = "/api/info/home",
}

export const StatsUrls = [
  { name: "/api/stats/accounts", value: "Registered accounts" },
  { name: "/api/stats/sessions", value: "Online players" },
  { name: "/api/stats/matches", value: "Running matches" },
  { name: "/api/stats/matches/players", value: "Total players in matches" },
  { name: "/api/stats/quests/active", value: "Active quests" },
  { name: "/api/stats/quests/completed", value: "Completed quests" },
  { name: "/api/stats/quests/rerolled", value: "Rerolled quests" },
  { name: "/api/stats/auctions", value: "Live auctions" },
  { name: "/api/stats/boosters", value: "Unopened boosters" },
  { name: "/api/stats/boosters/opened", value: "Opened boosters" },
  { name: "/api/stats/boosters/spent", value: "Total BFP spent on boosters" },
  { name: "/api/stats/cards", value: "Owned cards" },
  { name: "/api/stats/upgrades", value: "Owned card upgrades" },
  { name: "/api/stats/decks", value: "Unique decks" },
  { name: "/api/stats/mails", value: "Sent mails" },
  { name: "/api/stats/transactions", value: "Completed game transactions" },
  { name: "/api/stats/scratch", value: "Total scratch codes used" },
  {
    name: "/api/stats/auctions/watchers",
    value: "Total number of players watching auctions",
  },
  { name: "/api/stats/experience", value: "Total experience" },
  { name: "/api/stats/elo", value: "Total PvP ELO" },
  { name: "/api/stats/gold", value: "Total gold" },
  { name: "/api/stats/bfp", value: "Total BFP" },
  {
    name: "/api/stats/friendlist",
    value: "Total number of players in friends lists",
  },
  { name: "/api/stats/mutelist", value: "Total number of players on mute" },
  { name: "/api/stats/matches/1pve", value: "1-player PvE matches won" },
  { name: "/api/stats/matches/2pve", value: "2-player PvE matches won" },
  { name: "/api/stats/matches/4pve", value: "4-player PvE matches won" },
  { name: "/api/stats/matches/12pve", value: "12-player PvE matches won" },
  {
    name: "/api/stats/matches/1rpve",
    value: "1-player Random PvE matches won",
  },
  {
    name: "/api/stats/matches/2rpve",
    value: "2-player Random PvE matches won",
  },
  {
    name: "/api/stats/matches/4rpve",
    value: "4-player Random PvE matches won",
  },
  {
    name: "/api/stats/matches/1cpve",
    value: "1-player Community PvE matches won",
  },
  {
    name: "/api/stats/matches/2cpve",
    value: "2-player Community PvE matches won",
  },
  {
    name: "/api/stats/matches/4cpve",
    value: "4-player Community PvE matches won",
  },
  { name: "/api/stats/matches/1v1", value: "1v1 PvP matches won" },
  { name: "/api/stats/matches/2v2", value: "2v2 PvP matches won" },
  {
    name: "/api/stats/matches/c1v1",
    value: "1v1 PvP Community matches played",
  },
  {
    name: "/api/stats/matches/c2v2",
    value: "2v2 PvP Community matches played",
  },
  {
    name: "/api/stats/matches/c3v3",
    value: "3v3 PvP Community matches played",
  },
];

export enum AuctionsUrls {
  AUCTIONS = "/api/auctions",
  COUNT = "/api/auctions/count",
  EXPORT = "/api/auctions/export",
}

export enum LeaderboardsUrls {
  PVE_RANGES = "/api/leaderboards/ranges/pve",
  PVP_RANGES = "/api/leaderboards/ranges/pvp",
  PVP_LEADERBOARDS = "/api/leaderboards/pvp",
  PVP_COUNT = "/api/leaderboards/pvp-count",
  PVE_LEADERBOARDS = "/api/leaderboards/pve",
  PVE_COUNT = "/api/leaderboards/pve-count",
  PVE_MAPS = "/api/leaderboards/maps",
}
