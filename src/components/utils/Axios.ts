import axios from "axios";

const AxiosInstance = axios.create({
  baseURL: process.env.REACT_APP_BE_ROOT_URL,
});

AxiosInstance.interceptors.request.use(
  function (config) {
    config.headers = {
      ...config.headers,
    };
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

AxiosInstance.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (!error.response) {
      console.debug("network error");
    } else {
      switch (error.response.status) {
        case 400:
          break;
        case 401:
          break;
        case 403:
          break;
        case 404:
          break;
        case 500:
          break;
      }
    }
    return Promise.reject(error);
  }
);

export { AxiosInstance };
