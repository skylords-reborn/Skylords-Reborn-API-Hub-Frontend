import { useEffect, useState } from "react";
import { Spin } from "antd";
import { LeaderboardsKeys } from "./utils/Enums";
import { FrontendUrls } from "./utils/Urls";
import { getPageQueryParam, setPath } from "./utils/Functions";
import LeaderboardsMenu from "./leaderboards/LeaderboardsMenu";
import PvP from "./leaderboards/PvP";
import PvE from "./leaderboards/PvE";

const Leaderboards = () => {
  const [activeComponent, setActiveComponent] = useState(
    <Spin
      tip="LOADING LEADERBOARDS..."
      size="large"
      className="text-black-bold"
    />
  );

  useEffect(() => {
    setActiveComponent(<LeaderboardsMenu onClickCard={onClickCard} />);
  }, []);

  useEffect(() => {
    const link = getPageQueryParam();
    if (link === FrontendUrls.Leaderboards_1PvE) {
      setActiveComponent(Component(LeaderboardsKeys.PvE1));
      setPath(FrontendUrls.Leaderboards_1PvE);
    } else if (link === FrontendUrls.Leaderboards_2PvE) {
      setActiveComponent(Component(LeaderboardsKeys.PvE2));
      setPath(FrontendUrls.Leaderboards_2PvE);
    } else if (link === FrontendUrls.Leaderboards_4PvE) {
      setActiveComponent(Component(LeaderboardsKeys.PvE4));
      setPath(FrontendUrls.Leaderboards_4PvE);
    } else if (link === FrontendUrls.Leaderboards_12PvE) {
      setActiveComponent(Component(LeaderboardsKeys.PvE12));
      setPath(FrontendUrls.Leaderboards_12PvE);
    } else if (link === FrontendUrls.Leaderboards_1v1) {
      setActiveComponent(Component(LeaderboardsKeys.PvP1));
      setPath(FrontendUrls.Leaderboards_1v1);
    } else if (link === FrontendUrls.Leaderboards_2v2) {
      setActiveComponent(Component(LeaderboardsKeys.PvP2));
      setPath(FrontendUrls.Leaderboards_2v2);
    }
  }, []);

  const Component = (type: LeaderboardsKeys): JSX.Element => {
    let component: JSX.Element = <></>;
    if (type === LeaderboardsKeys.PvE1) {
      component = (
        <PvE
          type={1}
          setActiveComponent={setActiveComponent}
          onClickCard={onClickCard}
        />
      );
    } else if (type === LeaderboardsKeys.PvE2) {
      component = (
        <PvE
          type={2}
          setActiveComponent={setActiveComponent}
          onClickCard={onClickCard}
        />
      );
    } else if (type === LeaderboardsKeys.PvE4) {
      component = (
        <PvE
          type={4}
          setActiveComponent={setActiveComponent}
          onClickCard={onClickCard}
        />
      );
    } else if (type === LeaderboardsKeys.PvE12) {
      component = (
        <PvE
          type={12}
          setActiveComponent={setActiveComponent}
          onClickCard={onClickCard}
        />
      );
    } else if (type === LeaderboardsKeys.PvP1) {
      component = (
        <PvP
          type={"1"}
          setActiveComponent={setActiveComponent}
          onClickCard={onClickCard}
        />
      );
    } else if (type === LeaderboardsKeys.PvP2) {
      component = (
        <PvP
          type={"2"}
          setActiveComponent={setActiveComponent}
          onClickCard={onClickCard}
        />
      );
    } else {
      component = <p>Unknown</p>;
    }
    return component;
  };

  const onClickCard = (type: LeaderboardsKeys) => {
    setActiveComponent(Component(type));
    setPath(`?page=${type.toString()}`);
  };

  return activeComponent;
};

export default Leaderboards;
