import { useEffect, useState } from "react";
import { Form, Input, Button, Table, Tag, notification } from "antd";
import {
  NumberOutlined,
  ExportOutlined,
  SearchOutlined,
  ReloadOutlined,
} from "@ant-design/icons";
import axios, { CancelTokenSource } from "axios";
import { AuctionsTableColumns } from "./utils/TableColumns";
import { AuctionSearchFilters, AuctionsTableRow } from "./utils/Types";
import { AxiosInstance } from "./utils/Axios";
import { GetSpinProps, m } from "./utils/Functions";
import { AuctionsUrls } from "./utils/Urls";

const Auctions = () => {
  const resultsPerPage = 20;
  const [loading, setLoading] = useState(false);
  const [tableRows, setTableRows] = useState<AuctionsTableRow[]>([]);
  const [searchFilters, setSearchFilters] = useState<AuctionSearchFilters>({
    cardName: "",
    min: 0,
    max: 1000000,
  });
  const [currentPage, setCurrentPage] = useState(1);
  const [numberOfResults, setNumberOfResults] = useState(0);

  useEffect(() => {
    let mounted = true;
    const source = axios.CancelToken.source();
    (async () => {
      if (mounted) {
        await search(1, searchFilters, mounted, source);
      }
    })();
    return () => {
      mounted = false;
      source.cancel();
    };
  }, []);

  const onFinish = async (values: any) => {
    const filters = values as AuctionSearchFilters;
    setCurrentPage(1);
    setSearchFilters(filters);
    await search(1, filters);
  };

  const onChange = async (page: number, pageSize?: number | undefined) => {
    if (page && pageSize && searchFilters) {
      setCurrentPage(page);
      await search(page, searchFilters);
    }
  };

  const search = async (
    page: number,
    filters: AuctionSearchFilters,
    mounted: boolean | undefined = undefined,
    source: CancelTokenSource | undefined = undefined
  ) => {
    if (mounted !== undefined && mounted === false) {
      return;
    }

    setLoading(true);
    try {
      let result = await AxiosInstance.get(
        `${process.env.REACT_APP_BE_ROOT_URL}${AuctionsUrls.COUNT}?cardName=${filters.cardName}&min=${filters.min}&max=${filters.max}`,
        mounted !== undefined && source !== undefined
          ? { cancelToken: source.token }
          : {}
      );
      if (result && result.status === 200 && result.data) {
        const { count } = result.data;
        setNumberOfResults(count);

        result = await AxiosInstance.get(
          `${process.env.REACT_APP_BE_ROOT_URL}${AuctionsUrls.AUCTIONS}?cardName=${filters.cardName}&min=${filters.min}&max=${filters.max}&page=${page}&number=${resultsPerPage}`,
          mounted !== undefined && source !== undefined
            ? { cancelToken: source.token }
            : {}
        );
        if (result && result.status === 200 && result.data) {
          const rows = result.data as AuctionsTableRow[];
          if (rows) {
            for (let i = 0; i < rows.length; i++) {
              rows[i].key = rows[i].cardName + rows[i].auctionId;
            }
            setTableRows(rows);
          }
        }
      }
    } catch (error) {
      if (axios.isCancel(error) === false) {
        console.error(error);
      }
      setTableRows([]);
      setCurrentPage(1);
      setNumberOfResults(0);
    }
    setLoading(false);
  };

  const exportToCsv = async () => {
    try {
      const result = await AxiosInstance.get(
        `${process.env.REACT_APP_BE_ROOT_URL}${AuctionsUrls.EXPORT}`
      );
      if (result && result.status === 200 && result.data) {
        const element = document.createElement("a");
        const file = new Blob([result.data], {
          type: "text/csv",
        });
        element.href = URL.createObjectURL(file);
        element.download = "sr-auctions-export.csv";
        document.body.appendChild(element);
        element.click();
        element.remove();
      }
    } catch {
      notification.error(m("Failed to export auctions to CSV file."));
    }
  };

  const TitleComponent = (): JSX.Element => {
    return (
      <Form
        layout="inline"
        initialValues={{ cardName: "", min: 0, max: 1000000 }}
        onFinish={onFinish}
      >
        <Form.Item
          label={<label className="text-white">Card Name</label>}
          name="cardName"
          className="mt-1"
        >
          <Input type="text" />
        </Form.Item>
        <Form.Item
          label={<label className="text-white">Min. Price</label>}
          name="min"
          className="mt-1"
        >
          <Input type="number" />
        </Form.Item>
        <Form.Item
          label={<label className="text-white">Max. Price</label>}
          name="max"
          className="mt-1"
        >
          <Input type="number" />
        </Form.Item>
        <Form.Item
          label={<label className="text-white">Actions</label>}
          className="mt-1"
        >
          <Button
            title="Search"
            type="primary"
            htmlType="submit"
            className="ml-16px my-bg bg-border"
          >
            <SearchOutlined />
          </Button>
          <Button
            title="Export to CSV"
            type="primary"
            className="ml-1 my-bg bg-border"
            onClick={exportToCsv}
          >
            <ExportOutlined />
          </Button>
          <Button
            title="Refresh Table"
            type="primary"
            className="ml-1 my-bg bg-border"
            onClick={() => search(currentPage, searchFilters)}
          >
            <ReloadOutlined />
          </Button>
        </Form.Item>
      </Form>
    );
  };

  return (
    <Table
      title={() => <TitleComponent />}
      dataSource={tableRows as any}
      columns={AuctionsTableColumns}
      loading={loading ? GetSpinProps("AUCTIONS") : false}
      pagination={{
        className: "pagination my-bg text-white",
        onChange: onChange,
        pageSize: resultsPerPage,
        showSizeChanger: false,
        current: currentPage,
        total: numberOfResults,
        showTotal: () => (
          <Tag closable={false} title="Total number of results">
            <NumberOutlined /> {numberOfResults}
          </Tag>
        ),
        position: ["bottomCenter"],
      }}
    />
  );
};

export default Auctions;
