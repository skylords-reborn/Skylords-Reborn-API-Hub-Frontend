import { Layout, Image, Typography } from "antd";

const { Header } = Layout;
const { Link } = Typography;

const MyHeader = () => {
  return (
    <Header className="site-layout-background my-header my-bg">
      <Link href="https://skylords.eu" target="_blank">
        <Image
          src={"/logo.png"}
          className="logo"
          preview={false}
          alt="SR Logo"
        />
      </Link>
    </Header>
  );
};

export default MyHeader;
