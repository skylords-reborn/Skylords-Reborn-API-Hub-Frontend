import { useEffect, useState } from "react";
import { isMobile, isTablet } from "react-device-detect";
import { Layout, Menu, Affix } from "antd";
import {
  HomeOutlined,
  GlobalOutlined,
  OrderedListOutlined,
  CreditCardOutlined,
  FileOutlined,
} from "@ant-design/icons";
import { InfoUrls, FrontendUrls } from "../utils/Urls";
import { MenuKeys } from "../utils/Enums";
import { getPageQueryParam, setPath } from "../utils/Functions";
import Statistics from "../Statistics";
import Auctions from "../Auctions";
import Leaderboards from "../Leaderboards";
import Documentation from "../Documentation";
import MarkdownDocumentation from "../other/MarkdownDocumentation";

const { Sider } = Layout;
const HomePage = (
  <MarkdownDocumentation url={InfoUrls.HomeDocs} className="ml-1 mt-1" />
);

const MySider = ({ setActiveComponent }: any) => {
  const [collapsed, setCollapsed] = useState<boolean>();
  const [selectedKey, setSelectedKey] = useState<string>(MenuKeys.HOME);

  const onCollapse = (e: boolean) => {
    setCollapsed(e);
  };

  useEffect(() => {
    setActiveComponent(HomePage);
  }, [setActiveComponent]);

  useEffect(() => {
    const link = getPageQueryParam();
    if (link === FrontendUrls.Statistics) {
      setSelectedKey(MenuKeys.API_STATS);
      setActiveComponent(<Statistics />);
    } else if (link === FrontendUrls.Auctions) {
      setSelectedKey(MenuKeys.API_AUCTIONS);
      setActiveComponent(<Auctions />);
    } else if (
      link === FrontendUrls.Leaderboards ||
      link.endsWith("-pve") ||
      link.endsWith("-pvp")
    ) {
      setSelectedKey(MenuKeys.API_RANKINGS);
      setActiveComponent(<Leaderboards />);
    } else if (link === FrontendUrls.Documentation) {
      setSelectedKey(MenuKeys.DOCUMENTATION);
      setActiveComponent(<Documentation />);
    } else {
      setSelectedKey(MenuKeys.HOME);
      setActiveComponent(HomePage);
      setPath(FrontendUrls.Home);
    }
  }, []);

  return (
    <Sider
      collapsible={isMobile === false && isTablet === false}
      collapsed={isMobile === false && isTablet === false ? collapsed : true}
      onCollapse={
        isMobile === false && isTablet === false ? onCollapse : undefined
      }
      className="my-bg"
    >
      <Affix>
        <Menu
          theme="dark"
          defaultSelectedKeys={[MenuKeys.HOME]}
          selectedKeys={[selectedKey]}
          mode="inline"
          className="my-bg"
        >
          <Menu.Item
            key={MenuKeys.HOME}
            icon={<HomeOutlined />}
            onClick={() => {
              if (selectedKey !== MenuKeys.HOME) {
                setSelectedKey(MenuKeys.HOME);
                setActiveComponent(HomePage);
                setPath(FrontendUrls.Home);
              }
            }}
          >
            Home
          </Menu.Item>
          <Menu.Item
            key={MenuKeys.API_STATS}
            icon={<GlobalOutlined />}
            onClick={() => {
              if (selectedKey !== MenuKeys.API_STATS) {
                setActiveComponent(<Statistics />);
                setSelectedKey(MenuKeys.API_STATS);
                setPath(FrontendUrls.Statistics);
              }
            }}
          >
            Statistics
          </Menu.Item>
          <Menu.Item
            key={MenuKeys.API_AUCTIONS}
            icon={<CreditCardOutlined />}
            onClick={() => {
              if (selectedKey !== MenuKeys.API_AUCTIONS) {
                setActiveComponent(<Auctions />);
                setSelectedKey(MenuKeys.API_AUCTIONS);
                setPath(FrontendUrls.Auctions);
              }
            }}
          >
            Auctions
          </Menu.Item>
          <Menu.Item
            key={MenuKeys.API_RANKINGS}
            icon={<OrderedListOutlined />}
            onClick={() => {
              if (selectedKey !== MenuKeys.API_RANKINGS) {
                setActiveComponent(<Leaderboards />);
                setSelectedKey(MenuKeys.API_RANKINGS);
                setPath(FrontendUrls.Leaderboards);
              }
            }}
          >
            Leaderboards
          </Menu.Item>
          <Menu.Item
            key={MenuKeys.DOCUMENTATION}
            icon={<FileOutlined />}
            onClick={() => {
              if (selectedKey !== MenuKeys.DOCUMENTATION) {
                setActiveComponent(<Documentation />);
                setSelectedKey(MenuKeys.DOCUMENTATION);
                setPath(FrontendUrls.Documentation);
              }
            }}
          >
            Documentation
          </Menu.Item>
        </Menu>
      </Affix>
    </Sider>
  );
};

export default MySider;
