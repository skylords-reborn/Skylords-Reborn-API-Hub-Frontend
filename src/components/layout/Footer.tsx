import { Layout, Typography } from "antd";

const { Footer } = Layout;
const { Link } = Typography;

const MyFooter = () => {
  return (
    <Footer className="text-center my-bg text-white my-footer">
      <Link
        href="https://gitlab.com/skylords-reborn/Skylords-Reborn-API-Hub-Frontend"
        target="_blank"
      >
        Skylords Reborn ©2023 | Created Using Ant Design | Maintained by{" "}
        <a href="https://github.com/fiki574" target="_blank" rel="noreferrer">
          fiki574
        </a>
      </Link>
    </Footer>
  );
};

export default MyFooter;
