import { useEffect, useState } from "react";
import ReactMarkdown from "react-markdown";
import { notification } from "antd";
import axios from "axios";
import { AxiosInstance } from "../utils/Axios";

const MarkdownDocumentation = ({ url, className }: any) => {
  const [data, setData] = useState<string>();
  useEffect(() => {
    let mounted = true;
    const source = axios.CancelToken.source();
    AxiosInstance.get(url, { cancelToken: source.token })
      .then((result) => {
        const res = result.data as string;
        if (res && mounted) {
          setData(res);
        }
      })
      .catch((error) => {
        if (axios.isCancel(error) === false) {
          notification.error(error);
        }
      });
    return () => {
      mounted = false;
      source.cancel();
    };
  }, [url]);
  return data && data.length > 0 ? (
    <ReactMarkdown className={className}>{data}</ReactMarkdown>
  ) : (
    <></>
  );
};

export default MarkdownDocumentation;
