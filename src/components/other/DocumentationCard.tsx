import { Card, Image } from "antd";

const { Meta } = Card;

const DocumentationCard = ({ title, customKey, number, url }: any) => {
  const imgAlt = `${customKey} api`;
  const metaTitle = `External Documentation #${number}`;
  return (
    <Card
      onClick={() => window.open(`${process.env.REACT_APP_BE_ROOT_URL}${url}`)}
      title={title}
      key={customKey}
      hoverable
      className="my-card-2"
      cover={<Image alt={imgAlt} src="/images/swagger.jpg" preview={false} />}
    >
      <Meta
        title={metaTitle}
        description="Click here to open document page in new tab."
      />
    </Card>
  );
};

export default DocumentationCard;
