import "antd/dist/antd.css";
import "./App.css";

import { useState } from "react";
import { isIE } from "react-device-detect";
import { Alert, Layout, Spin } from "antd";
import MyFooter from "./components/layout/Footer";
import MySider from "./components/layout/Sider";
import MyHeader from "./components/layout/Header";

const { Content } = Layout;

function App() {
  const [activeComponent, setActiveComponent] = useState(<Spin />);
  return (
    <Layout className="my-layout">
      <MySider setActiveComponent={setActiveComponent} />
      <Layout className="site-layout">
        <MyHeader />
        <Content>
          <div className="site-layout-background">
            {isIE ? (
              <Alert
                message="Error"
                showIcon
                description="Internet Explorer not supported!"
                type="error"
              />
            ) : (
              activeComponent
            )}
          </div>
        </Content>
        <MyFooter />
      </Layout>
    </Layout>
  );
}

export default App;
