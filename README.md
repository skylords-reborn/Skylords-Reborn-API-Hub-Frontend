![Skylords Reborn Logo](./public/logo.png)

## License

This project is open source and available under GNU General Public License v3.0. See [LICENSE](./LICENSE) for more information.

## Disclosure

EA has not endorsed and does not support this product.
